def fibonacci(n):
    """Return pair of fibonacci numbers, F(n) and F(n - 1)"""
    if n <= 1:
        return n, 0
    else:
        (a, b) = fibonacci(n - 1)
        return a + b, a
