import copy

def find_subsets(my_set):
    current_subsets = [[]]
    __find_subsets_rec(current_subsets, my_set)
    return current_subsets


def __find_subsets_rec(current_subsets, my_set):
    if len(my_set) == 0:
        return current_subsets
    else:
        __merge_subsets(current_subsets, my_set[0])
        __find_subsets_rec(current_subsets, my_set[1:])


def __merge_subsets(current_subsets, elem):
    for i in range(len(current_subsets)):
        new_elem = copy.copy(current_subsets[i])
        new_elem.append(elem)
        current_subsets.append(new_elem)


def __print_list(lst):
    for s in lst:
        print(*s)


if __name__ == "__main__":
    print("Set with one elem:")
#    __print_list(find_subsets(["a"]))
    print("-----------------------")
    print("Set with two elems:")
    lst = find_subsets(["a", "b"])
    __print_list(lst)
    print("Set with three elems:")
    lst = find_subsets(["a", "b", "c"])
    __print_list(lst)
